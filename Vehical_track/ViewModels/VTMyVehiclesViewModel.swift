//
//  VTMyVehiclesViewModel.swift
//  Vehical_track
//
//  Created by Saleel Uday Karkhanis on 09/10/21.
//

import Foundation

protocol VTMyVehiclesListDelegate {
    func vehicleListDidFinishLoading()
    func vehicleListDidReceiveError(error: VTError)
}

class VTMyVehiclesViewModel {
    var myVehicles = [VTVehicle]()
    var delegate: VTMyVehiclesListDelegate?
    var dataLoader: VTDataLoader
    var endPoint: VTEndPoint = .vehicleList
    
    init(delegate: VTMyVehiclesListDelegate?, dataLoader: VTDataLoader) {
        self.delegate = delegate
        self.dataLoader = dataLoader
    }

    func getMyVehiclesData() {
        self.dataLoader.loadData(endPoint: endPoint, type: [VTVehicle].self, onComplete: { [weak self] (myVehicles) in
            self?.myVehicles = myVehicles
            self?.delegate?.vehicleListDidFinishLoading()
        }) { [weak self] (error) in
            self?.delegate?.vehicleListDidReceiveError(error: error)
        }
    }
}
