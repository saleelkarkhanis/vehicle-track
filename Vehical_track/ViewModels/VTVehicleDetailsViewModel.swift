//
//  VTVehicleDetailsViewModel.swift
//  Vehical_track
//
//  Created by Saleel Uday Karkhanis on 09/10/21.
//

import Foundation

class VTVehicleDetailsViewModel {

    private var vehicle: VTVehicle
    
    lazy var vehicleName: String = {
        return "\(vehicle.type) (\(vehicle.make))"
    }()
    
    lazy var licenceNumber: String = {
        return vehicle.licencePlateNumber
    }()
    
    lazy var statusText: String = {
        var status = "INACTIVE"
        if vehicle.isActive {
            status = vehicle.isAvailable ? "AVAILABLE" : "BOOKED"
        }
        return status
    }()
    
    lazy var seatingCapacity: String =  {
        return "\(vehicle.seatingCapasity) seats"
    }()
    
    lazy var remainingMilage: String? =  {
        if let milage = vehicle.remainingMilage {
            return "\(milage) Km/Hr"
        }
        return nil
    }()
    
    lazy var remainingDistance: String? =  {
        if let range = vehicle.remainingRange {
            return "\(Int(range/1000)) Km"
        }
        return nil
    }()
    
    lazy var photoURL: URL? = {
        return URL(string: vehicle.photoURL)
    }()
    
    init(vehicle: VTVehicle) {
        self.vehicle = vehicle
    }
}
