//
//  VTVehicle.swift
//  Vehical_track
//
//  Created by Saleel Uday Karkhanis (Industrial & Engineering Services (I&ES)) on 09/10/21.
//

import Foundation
import CoreLocation

struct VTVehicle: Codable {
    
    var id: Int
    var make: String
    var type: String
    var photoURL: String
    var transmissionMode: String?
    var licencePlateNumber: String
    var currentLatitude: Double?
    var currentLogitude: Double?
    var remainingMilage: Int?
    var remainingRange: Int?
    var isActive: Bool
    var isAvailable: Bool
    var seatingCapasity: Int
    
    enum CodingKeys: String, CodingKey {
        case id
        case make = "vehicle_make"
        case type = "vehicle_type"
        case photoURL = "vehicle_pic_absolute_url"
        case transmissionMode = "transmission_mode"
        case licencePlateNumber = "license_plate_number"
        case currentLatitude = "lat"
        case currentLogitude = "lng"
        case remainingMilage = "remaining_mileage"
        case remainingRange = "remaining_range_in_meters"
        case isActive = "is_active"
        case isAvailable = "is_available"
        case seatingCapasity = "seating_capasity"
    }

    var coordinates: CLLocationCoordinate2D? {
        if let latitude = currentLatitude, let longitude = currentLogitude {
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        return nil
    }
}
