//
//  VTAppConstants.swift
//  Vehical_track
//
//  Created by Saleel Uday Karkhanis on 09/10/21.
//

import Foundation

import UIKit

struct VTAppConstants {
    
    struct FontFamilies {
        static let avenierBook = "Avenir-Book"
        static let avenierMedium = "Avenir-Medium"
    }

    struct FontSizes {
        static let small: CGFloat = 14.0
        static let medium: CGFloat = 16.0
    }

    struct PageTitles {
        static let myVehicles = "My Vehicles"
    }
    
    struct NibNames {
        static let vehicleDetailsView = "VehicleDetailsView"
        static let fullScreenErrorView = "FullScreenErrorView"
    }
    
    struct ErrorMessages {
        static let genericErrorTitle = "Something went wrong"
        static let genericErrorMessage = "We are facing some issues. Please try again after some time."
        static let parsinngErrorMessage = "We are Unable to load vehicle data."
        static let mapLoadError = "Location not available. Unable to load map."
    }
    
    struct Identifiers {
        static let customMapPin = "customMapPin"
    }

    struct ImageNames {
        static let mapAnnotation = "map_annotation"
        static let carPlaceholder = "Car_Placeholder"
    }
}
    
