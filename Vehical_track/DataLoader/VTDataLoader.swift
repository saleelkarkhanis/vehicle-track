//
//  VTDataLoader.swift
//  Vehical_track
//
//  Created by Saleel Uday Karkhanis on 09/10/21.
//

import Foundation

enum VTError: Error {
    case parsingError
    case genericError
    case mapLoadError
    
    func errorMessage() -> String {
        switch self {
        case .parsingError:
            return VTAppConstants.ErrorMessages.parsinngErrorMessage
        case .genericError:
            return VTAppConstants.ErrorMessages.genericErrorMessage
        case .mapLoadError:
            return VTAppConstants.ErrorMessages.mapLoadError
        }
    }
}

enum VTEndPoint {
    case unknown
    case vehicleList
    case vehicleListError
    
    func path() -> String {
        switch self {
        case .vehicleList:
            return "vehicles_list"
        case .vehicleListError:
            return "vehicles_list_error"
        case .unknown:
            return "unknown"
        }
    }
}

protocol VTDataLoader {
    func loadData<T>(endPoint: VTEndPoint, type: T.Type, onComplete: @escaping (T) -> Void, onError: @escaping (VTError) -> Void ) where T: Codable
}

// This class is to read Data from Local JSON File
class VTJSONLoader: VTDataLoader {
    
    static let shared = VTJSONLoader()
    private init() { }
    
    func loadData<T>(endPoint: VTEndPoint, type: T.Type, onComplete: @escaping (T) -> Void, onError: @escaping (VTError) -> Void ) where T: Codable {
        
        guard let path = Bundle.main.path(forResource: endPoint.path(), ofType: "json") else {
            onError(VTError.genericError)
            return
        }

        // Read data from Local JSON file and map it to Generic Model class
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let vehiclesList = try JSONDecoder().decode(T.self, from: data)
            onComplete(vehiclesList)
        } catch (let error){
            debugPrint("Error while parsing - \(error.localizedDescription)")
            onError(VTError.parsingError)
        }
    }
    
}
