//
//  UIViewController+helper.swift
//  Vehical_track
//
//  Created by Saleel Uday Karkhanis on 09/10/21.
//

import UIKit
import MapKit

extension AppDelegate {
    func customizeNavigationBar() {
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor(named: "PrimaryBackground")
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: VTAppConstants.FontFamilies.avenierMedium, size: VTAppConstants.FontSizes.medium)!, NSAttributedString.Key.foregroundColor: UIColor.white]
    }
}

extension UIViewController {
    func navigationItem(withTitle title: String) {
        self.navigationItem.title = title
        
        // Remove back button title from navigationBar
        let leftItem: UIBarButtonItem = UIBarButtonItem()
        leftItem.title = ""
        self.navigationItem.backBarButtonItem = leftItem
    }
}

extension UIView {
    func showError(error: VTError) {
        let errorView = Bundle.main.loadNibNamed(VTAppConstants.NibNames.fullScreenErrorView, owner: self, options: nil)?.first as! FullScreenErrorView
        errorView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        errorView.configureErrorView(error: error)
        self.addSubview(errorView)
        self.bringSubviewToFront(errorView)
    }
    
    func hideError() {
        for subview in self.subviews where subview is FullScreenErrorView {
            subview.removeFromSuperview()
        }
    }
    
    func remooveAllSubviews() {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
    }
}

extension MKMapView {
    func setupRegion(coordinates: CLLocationCoordinate2D) {
        let span = MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001)
        let region = MKCoordinateRegion(center: coordinates, span: span)
        setRegion(region, animated: true)
    }
    
    func addAnnotationFor(vehicle: VTVehicle, atCoordinates coordinates: CLLocationCoordinate2D) {
        let annotation = MKPointAnnotation()
        annotation.title = vehicle.licencePlateNumber
        setAddressForAnnotation(annotation: annotation, atCoordinates: coordinates)
        annotation.coordinate = coordinates
        addAnnotation(annotation)
    }
    
    private func setAddressForAnnotation(annotation: MKPointAnnotation, atCoordinates coordinates: CLLocationCoordinate2D) {
        let ceo: CLGeocoder = CLGeocoder()
        let loc: CLLocation = CLLocation(latitude:coordinates.latitude, longitude: coordinates.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler: {(placemarks, error) in
            guard let placemark = placemarks?.first, error == nil else {
                return
            }
            var addressString : String = ""
            if let sublocality = placemark.subLocality {
                addressString += (sublocality + ", ")
            }

            if let locality = placemark.locality {
                addressString += locality
            }
            annotation.subtitle = addressString
        })
    }
}

