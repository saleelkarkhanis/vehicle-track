//
//  VTMyVehiclesViewController.swift
//  Vehical_track
//
//  Created by Saleel Uday Karkhanis on 09/10/21.
//

import UIKit
import MapKit
import CoreLocation

class VTMyVehiclesViewController: UIViewController {
    
    var viewModel: VTMyVehiclesViewModel?
    @IBOutlet weak var vehicleDetailsScroillView: UIScrollView!
    @IBOutlet weak var vehicleDetailsContainerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var bookButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem(withTitle: VTAppConstants.PageTitles.myVehicles)
        viewModel = VTMyVehiclesViewModel(delegate: self, dataLoader: VTJSONLoader.shared)
        setupInitialUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel?.getMyVehiclesData()
    }
    
    @IBAction func pageControlDidChangeValue(_ sender: UIPageControl) {
        let index = pageControl.currentPage
        let xCoordinate = CGFloat(index) * vehicleDetailsContainerView.frame.width
        vehicleDetailsScroillView.setContentOffset(CGPoint(x: xCoordinate, y: 0 ), animated: true)
        setMapForPage(pageNumber: index)
    }
    
    @IBAction func refreshButtonActioon(_ sender: UIBarButtonItem) {
        viewModel?.getMyVehiclesData()
    }

    private func setupInitialUI() {
        bookButton.layer.cornerRadius = bookButton.frame.width/2
        pageControl.numberOfPages = 0
    }

    private func setupVehicleDetailsScrollView() {
        guard let myVehicles = viewModel?.myVehicles, myVehicles.count > 0 else {
            self.view.showError(error: .parsingError) // Show Error if there are no Vehicles
            return
        }

        for (index, vehicle) in myVehicles.enumerated() {
            let vehicleDetailsView = Bundle.main.loadNibNamed(VTAppConstants.NibNames.vehicleDetailsView, owner: self, options: nil)?.first as! VehicleDetailsView
            vehicleDetailsView.frame = CGRect(x: vehicleDetailsContainerView.frame.width * CGFloat(index), y: 0,
                                              width: vehicleDetailsContainerView.frame.width, height: vehicleDetailsContainerView.frame.height)
            vehicleDetailsView.configureFromModel(model: VTVehicleDetailsViewModel(vehicle: vehicle))
            vehicleDetailsScroillView.addSubview(vehicleDetailsView)
        }
        vehicleDetailsScroillView.contentSize = CGSize(width: vehicleDetailsContainerView.frame.width * CGFloat(myVehicles.count),
                                                       height: vehicleDetailsContainerView.frame.height)
        pageControl.numberOfPages = myVehicles.count
        pageControl.currentPage = 0
    }

    private func setupMapViewWithAnotations() {
        guard let myVehicles = viewModel?.myVehicles else { return }
        for (index, vehicle) in  myVehicles.enumerated() {
            guard let coordinates = vehicle.coordinates else {
                if index == 0 { // Show error on map only of Coordiates not available for First vehicle
                    mapView.showError(error: .mapLoadError)
                }
                return
            }
            mapView.addAnnotationFor(vehicle: vehicle, atCoordinates: coordinates)
            if index == 0 { // Show initial regionn for First vehicle
                mapView.setupRegion(coordinates: coordinates)
            }
        }
    }

    private func setMapForPage(pageNumber: Int) {
        guard let vehicle = viewModel?.myVehicles[pageNumber], let coordinates = vehicle.coordinates else {
            mapView.showError(error: .mapLoadError)
            return
        }
        mapView.hideError()
        mapView.setupRegion(coordinates: coordinates)
    }
}

// MARK: VehicleListDelegate methods
extension VTMyVehiclesViewController: VTMyVehiclesListDelegate {
    func vehicleListDidFinishLoading() {
        setupVehicleDetailsScrollView()
        setupMapViewWithAnotations()
    }

    func vehicleListDidReceiveError(error: VTError) {
        self.view.showError(error: error)
    }
}

// MARK: Scrollview Delegate methods
extension VTMyVehiclesViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = pageNumber
        setMapForPage(pageNumber: pageNumber)
    }
}

// MARK: Mapview Delegate methods
extension VTMyVehiclesViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: VTAppConstants.Identifiers.customMapPin)
        if annotationView == nil  {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: VTAppConstants.Identifiers.customMapPin)
        } else {
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: VTAppConstants.ImageNames.mapAnnotation)
        annotationView?.canShowCallout = true
        return annotationView
    }
}

