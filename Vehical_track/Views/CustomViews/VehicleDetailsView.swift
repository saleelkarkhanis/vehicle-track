//
//  CustomView.swift
//  temp
//
//  Created by Saleel Uday Karkhanis on 08/10/21.
//

import UIKit
import SDWebImage

class VehicleDetailsView: UIView {
    
    @IBOutlet weak var vehicleNameLabel: UILabel!
    @IBOutlet weak var licenceNumberLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var remainingDistanceView: UIButton!
    @IBOutlet weak var remainingMilageView: UIButton!
    @IBOutlet weak var seatingCapasityView: UIButton!
    @IBOutlet weak var vehicleImageview: UIImageView!
    
    func configureFromModel(model: VTVehicleDetailsViewModel) {
        remainingDistanceView.isHidden = true
        remainingMilageView.isHidden = true
        vehicleNameLabel.text = model.vehicleName
        licenceNumberLabel.text = model.licenceNumber
        statusLabel.text = model.statusText
        if let remainingMilage = model.remainingMilage {
            remainingMilageView.isHidden = false
            remainingMilageView.setTitle(remainingMilage, for: .normal)
        }
        if let remainingDistance = model.remainingDistance {
            remainingDistanceView.isHidden = false
            remainingDistanceView.setTitle(remainingDistance, for: .normal)
        }
        seatingCapasityView.setTitle(model.seatingCapacity, for: .normal)
        
        let placeholderImage = UIImage(named: VTAppConstants.ImageNames.carPlaceholder)
        vehicleImageview.image = placeholderImage
        if let url = model.photoURL {
            vehicleImageview.sd_setImage(with: url, placeholderImage: placeholderImage)
        }
    }
}
