//
//  FullScreenErrorView.swift
//  TMDB
//
//  Created by Saleel Uday Karkhanis on 20/09/21.
//

import UIKit

class FullScreenErrorView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    
    func configureErrorView(error: VTError) {
        titleLabel.text = VTAppConstants.ErrorMessages.genericErrorTitle
        messageLabel.text = error.errorMessage()
    }
}
