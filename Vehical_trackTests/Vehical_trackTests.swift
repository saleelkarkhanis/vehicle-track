//
//  Vehical_trackTests.swift
//  Vehical_trackTests
//
//  Created by Saleel Uday Karkhanis on 09/10/21.
//

import XCTest
@testable import Vehical_track

class VehicleTrackerTest: XCTestCase {

    var viewModel: VTMyVehiclesViewModel!

    override func setUp() {
       viewModel = VTMyVehiclesViewModel(delegate: nil, dataLoader: VTJSONLoader.shared)
    }

    override func tearDown() {}

    func testDataLoadSuccess() {
        // API successful with correct Data
        viewModel.getMyVehiclesData()
        XCTAssertTrue(viewModel.myVehicles.count == 3, "Wrong vehicle count")
    }

    func testDataLoadFailure() {
        // API successful with correct Data
        viewModel.endPoint = .vehicleListError
        viewModel.getMyVehiclesData()
        XCTAssertTrue(viewModel.myVehicles.count == 0, "Wrong vehicle count")
        
        viewModel.endPoint = .unknown
        viewModel.getMyVehiclesData()
        XCTAssertTrue(viewModel.myVehicles.count == 0, "Wrong vehicle count")
    }
    
    func testVehicleDetailsViewModel() {
        viewModel.getMyVehiclesData()
        if let vehicle = viewModel.myVehicles.first {
            let vehicleDetailsModel = VTVehicleDetailsViewModel(vehicle: vehicle)
            XCTAssertTrue(vehicleDetailsModel.licenceNumber == "7LVF807", "Wrong Licence Number")
            XCTAssertTrue(vehicleDetailsModel.statusText == "AVAILABLE", "Wrong Status")
            XCTAssertTrue(vehicleDetailsModel.remainingDistance == "146 Km", "Wrong distance")
            XCTAssertTrue(vehicleDetailsModel.remainingMilage == "91 Km/Hr", "Wrong Milage")
            XCTAssertTrue(vehicleDetailsModel.seatingCapacity == "6 seats", "Wrong Seating Capasity")
        }
        
        if let anotherVehicle = viewModel.myVehicles.last {
            let vehicleDetailsModel = VTVehicleDetailsViewModel(vehicle: anotherVehicle)
            XCTAssertNil(vehicleDetailsModel.remainingDistance, "Distance should be nil")
        }
    }
    
    func testViewControllerSuccess() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "myvehiclesviewcontroller") as! VTMyVehiclesViewController
        viewController.loadView()
        viewController.viewDidLoad()
        viewController.viewDidAppear(false)
        XCTAssertTrue(viewController.pageControl.numberOfPages == 3, "Wrong numer of pages in pageConntrol")
        XCTAssertTrue(viewController.vehicleDetailsScroillView.subviews.count == 3, "Wrong number of subviews in Scrollview")
        XCTAssertTrue(viewController.mapView.annotations.count == 2, "Wrong number of Annnotations on Map")  
    }
    
    func testViewControllerFailure() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "myvehiclesviewcontroller") as! VTMyVehiclesViewController
        viewController.loadView()
        viewController.viewDidLoad()
        viewController.viewModel?.endPoint = .unknown
        viewController.viewDidAppear(false)
        XCTAssertTrue(viewController.pageControl.numberOfPages == 0, "Wrong numer of pages in pageConntrol")
        XCTAssertTrue(viewController.vehicleDetailsScroillView.subviews.count == 0, "Wrong number of subviews in Scrollview")
        XCTAssertTrue(viewController.mapView.annotations.count == 0, "Wrong number of Annnotations on Map")
    }
}
